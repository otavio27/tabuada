#!/usr/bin/env bash

#====================================
# Tabuada feito em Shell Script
# Autor: Otávio Will
# Versão: 1.2
#====================================

#====================================
# Colors
#====================================
Br="\033[1;37m"
Cz="\033[0;37m"
Rd="\033[1;31m"
Vd="\033[1;32m"
Cy="\033[0;36m"
Fm="\e[0m"
#====================================

#====================================

Menu () {

while true; do
	clear
	echo ""
	sleep 0.1s; echo -e  ${Cy}"  ████████╗ █████╗ ██████╗ ██╗   ██╗ █████╗ ██████╗  █████╗"${Fm} 
	sleep 0.1s; echo -e  ${Cy}"  ╚══██╔══╝██╔══██╗██╔══██╗██║   ██║██╔══██╗██╔══██╗██╔══██╗"${Fm}
    sleep 0.1s; echo -e  ${Cy}"     ██║   ███████║██████╔╝██║   ██║███████║██║  ██║███████║"${Fm}
    sleep 0.1s; echo -e  ${Cy}"     ██║   ██╔══██║██╔══██╗██║   ██║██╔══██║██║  ██║██╔══██║"${Fm}
    sleep 0.1s; echo -e  ${Cy}"     ██║   ██║  ██║██████╔╝╚██████╔╝██║  ██║██████╔╝██║  ██║"${Fm}
    sleep 0.1s; echo -e  ${Cy}"     ╚═╝   ╚═╝  ╚═╝╚═════╝  ╚═════╝ ╚═╝  ╚═╝╚═════╝ ╚═╝  ╚═╝"${Fm}
	echo ""
	echo -ne ${Br}"DIGITE UM NÚMERO. R: "${Fm}
	read dig

	if  [[ "$dig" =~ [a-zA-Z] ]]; then 
	    echo -ne "\n${Rd}SÓ É ACEITO NÚMEROS!\n\n${Br}RETORNAR AO INICIO?${Fm}\n${Br}[S/N]: ${Fm}"
	    read inicio 
    
	    if [[ "${inicio,,}" == @(s|sim) ]]; then
	      echo -e ${Br}"Retornando..."${Fm}
	      sleep 1s
	      Menu
	    else
		    echo -e ${Rd}"Saindo..."${Fm}
		    sleep 1s
		    clear
		    exit
	    fi
    else
       	Tabuada_Fun $dig
    fi
done

}

Tabuada_Fun () {

dig=$1

	echo ""
	echo -e ${Rd}"============================"${Fm}   
	echo -e ${Rd}"| TABUADA DE:  ====>  |${Fm} ${Br}$dig${Fm}${Rd} |"${Fm}
	echo -e ${Rd}"============================"${Fm}


	for (( t=1; t<11; t++ )); do
		echo ""
		sleep 0.3; echo -ne ${Br}"$dig"${Fm}; sleep 0.3;  echo -ne "" ${Br}"X"${Fm} ""; sleep 0.3;  echo -ne ${Br}"$t"${Fm}; sleep 0.3;  echo -ne "" ${Br}"="${Fm} ""; sleep 0.3;  echo -ne ${Cy}"$[$dig*$t]"${Fm}
		echo ""
	done
	echo ""
	echo -ne ${Br}"Voltar ao inicio? [S/N] R: "${Fm}
	read ini
	ini=${ini^^}

	if [ "$ini" == "S" ]; then
		echo -e ${Br}"Retornando..."${Fm}
		sleep 1s
	else
		echo -e ${Rd}"Saindo..."${Fm}
		sleep 1s
		clear
		exit
	fi
}

Menu
#=====================Fim da tabuada=====================